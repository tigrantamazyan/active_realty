@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 text-center">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Welcome to <b> images property</b></h3>
                    </div>
                    <div class="panel-body">
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cum debitis delectus doloremque facere. Debitis dolores doloribus earum est fugiat, ipsum maiores mollitia perspiciatis quae quam quibusdam quidem quos reiciendis voluptates!
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection