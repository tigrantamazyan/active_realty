@extends('layouts.app')
@section('content')
    <header class="main_header">
        <div class="header_content">
            <h1>
                About Us
            </h1>
        </div>
    </header>
    <div class="container">
        <h2 class=" text-center">What we do</h2>
        <hr class="bg-success">
        <p>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium labore maiores molestias nesciunt non ratione. Animi dignissimos eligendi ex, illum incidunt molestiae non numquam! Ipsam nisi quis quo reprehenderit tenetur.
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium labore maiores molestias nesciunt non ratione. Animi dignissimos eligendi ex, illum incidunt molestiae non numquam! Ipsam nisi quis quo reprehenderit tenetur.
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium labore maiores molestias nesciunt non ratione. Animi dignissimos eligendi ex, illum incidunt molestiae non numquam! Ipsam nisi quis quo reprehenderit tenetur.
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium labore maiores molestias nesciunt non ratione. Animi dignissimos eligendi ex, illum incidunt molestiae non numquam! Ipsam nisi quis quo reprehenderit tenetur.
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium labore maiores molestias nesciunt non ratione. Animi dignissimos eligendi ex, illum incidunt molestiae non numquam! Ipsam nisi quis quo reprehenderit tenetur.
        </p>
        <hr class="bg-success">
    </div>
@endsection

