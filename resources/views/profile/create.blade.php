@extends('layouts.app')

@section('content')
    <div class="container">
<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="card">
            <a class="col-1" href="{{ URL::previous() }}"><i class="fa fa-arrow-circle-left" style="font-size:36px"></i></a>
            <h1 class="text-center text-info">Create Property</h1>
            <form class="form-horizontal" action="{{route('store')}}" method="post" enctype="multipart/form-data">
                <input type="hidden" value="{{ csrf_token() }}" name="_token">
                    <div class="form-group">
                        @if ($errors->any())
                            <div class="col-md-6 alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="name">Name</label>
                        <div class="col-md-6">
                            <input id="name" name="name" placeholder="Name" class="form-control input-md" type="text" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="price">Price</label>
                        <div class="col-md-6">
                            <input id="price" name="price" placeholder="Price" class="form-control input-md" type="number" step="50" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="size">Size</label>
                        <div class="col-md-6">
                            <input id="size" name="size" placeholder="Size" class="form-control input-md" type="number" step="5" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="bedrooms">Bedrooms</label>
                        <div class="col-md-6">
                            <input id="bedrooms" name="bedrooms" placeholder="Bedrooms" class="form-control input-md" type="number" step="1" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="location">Location</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" id="location" name="location" placeholder="Location" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="description">Description</label>
                        <div class="col-md-6">
                            <textarea id="description" name="description" placeholder="Property Description" class="form-control input-md" type="text" required></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="image">Images</label>
                        <div class="col-md-6">
                            <input class="btn btn-primary" id="image" name="images[]" type="file" multiple required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="video">Video</label>
                        <div class="col-md-6">
                            <input class="btn btn-primary" id="video" name="video" type="file">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12 text-center">
                            <input type="submit" class="btn btn-success" value="Create" name="submit">
                        </div>
                    </div>
            </form>
            </div>
        </div>
    </div>
</div>
@endsection
