@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                   <h1>{{ $user->name }}</h1>
                    <p>{{$user->email}}</p>
                    <a href="{{url('profile/create')}}">Create Property</a>
                </div>
            </div>
            @foreach($properties as $property)
                <div class="card mb-3">
                    <div class="card-header">{{$property->name}}</div>
                    <img class="card-img-top" src="{{asset('uploads/images/'.$property->photos[0]->name)}}" style="height: 150px" alt="Card image cap">
                    <div class="card-body">
                        <p class="card-text">{{$property->description}}</p>
                        <p class="card-text">{{$property->price}}</p>
                        <p class="card-text">{{$property->location}}</p>
                        <p class="card-text"><small class="text-muted">{{$property->created_at}}</small><a href="{{route('edit',['id'=>$property->id])}}" class="btn btn-info float-right" >Edit</a></p>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>
@endsection
