@extends('layouts.app')
@section('content')
    <div class="col-md-6">
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#myCarousel" data-slide-to="1"></li>
                <li data-target="#myCarousel" data-slide-to="2"></li>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner">
                <div class="item active">
                    <img src="https://picsum.photos/200/200" alt="Los Angeles" style="width:100%;height: 400px">
                </div>

                <div class="item">
                    <img src="https://picsum.photos/200/200" alt="Chicago" style="width:100%;height: 200px">
                </div>

            </div>

            <!-- Left and right controls -->
            <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                <span class="fas fa-arrow-left"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#myCarousel" data-slide="next">
                <span class="fas fa-arrow-right"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
@endsection