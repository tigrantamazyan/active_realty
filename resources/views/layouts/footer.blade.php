<footer class="page-footer">
    <div id="footer">
        <div class="footer">
            <div class="container">
                <div class="row ">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                        <div class="ft-logo col-md-2">
                            <img src="{{asset('uploads/images/logo.png')}}" style="width: 100%;height:auto" alt="">
                        </div>
                    </div>
                </div>
                <hr class="footer-line">
                <div class="row ">
                    <!-- footer-about -->
                    <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-6 ">
                        <div class="footer-widget ">
                            <div class="footer-title">Company</div>
                            <ul class="list-unstyled">
                                <li><a href="{{url('about')}}">About</a></li>
                                <li><a href="#">Contact Us</a></li>
                                <li><a href="#">Legal & Privacy</a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- /.footer-about -->
                    <!-- footer-links -->
                    <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-6 ">
                        <div class="footer-widget ">
                            <div class="footer-title">Quick Links</div>
                            <ul class="list-unstyled">
                                <li><a href="{{url('house-details')}}">Details of House</a></li>
                                <li><a href="{{url('video-property')}}">Videos of Property</a></li>
                                <li><a href="{{url('site-plans')}}">Site Plans</a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- /.footer-links -->
                    <!-- footer-links -->
                    <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-6 ">
                        <div class="footer-widget ">
                            <div class="footer-title">Social</div>
                            <ul class="list-unstyled">
                                <li><a href="#">Twitter</a></li>
                                <li><a href="#">Google +</a></li>
                                <li><a href="#">Linked In</a></li>
                                <li><a href="#">Facebook</a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- /.footer-links -->
                    <!-- footer-links -->
                    <div class="col-xl-3 col-lg-3 col-md-12 col-sm-6 col-6 ">
                        <div class="footer-widget ">
                            <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Ffacebook&tabs=timeline&width=340&height=500&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" width="340" height="130" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</footer>