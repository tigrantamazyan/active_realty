@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-3 col-md-3">
                <div class="panel-group" id="accordion">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a href="{{url('site-plans')}}"><span class="icon"><i class="fas fa-columns"></i></span>Site Plan</a>
                            </h4>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a href="{{url('apartment')}}"><span class="icon"><i class="fas fa-building"></i></span>Apartment Plans </a>
                            </h4>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a href="{{url('geology-report')}}"><span class="icon"><i class="fas fa-atlas"></i></span>Geology Report </a>
                            </h4>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a href="{{url('seismic-report')}}"><span class="icon"><i class="fas fa-vihara"></i></span>Seismic Report </a>
                            </h4>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a href="{{url('survey-topo')}}"><span class="icon"><i class="fas fa-poll"></i></span>Survey & Topo </a>
                            </h4>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-6">
                <div class="well">
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto blanditiis consequuntur eos fuga molestias non perspiciatis repellat veritatis. Architecto dolorem enim eum, fugit iusto laudantium minus obcaecati perspiciatis sapiente soluta.
                    </p>
                    <video width="100%" height="240" controls>
                        <source src="{{ asset('\uploads\videos\Video.mp4') }}" type="video/mp4">
                        Your browser does not support the video tag.
                    </video>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto blanditiis consequuntur eos fuga molestias non perspiciatis repellat veritatis. Architecto dolorem enim eum, fugit iusto laudantium minus obcaecati perspiciatis sapiente soluta.
                    </p>
                </div>
            </div>
            <div class="col-md-3 col-sm-3">
                <div class="well">
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque commodi cumque dicta dolores fugit nisi obcaecati optio quia quis totam! Cupiditate harum ipsum, molestiae non officia repudiandae sunt velit voluptas.
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque commodi cumque dicta dolores fugit nisi obcaecati optio quia quis totam! Cupiditate harum ipsum, molestiae non officia repudiandae sunt velit voluptas.
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque commodi cumque dicta dolores fugit nisi obcaecati optio quia quis totam! Cupiditate harum ipsum, molestiae non officia repudiandae sunt velit voluptas.
                    </p>
                </div>
            </div>
        </div>
        <div class="row">
                <div class="col-md-3 col-sm-3">
                    <div class="well">
                    <div class="panel-group">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a href="{{url('images-property')}}"><span class="icon"><i class="far fa-images"></i></span>Images Of Property</a>
                                </h4>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a href="{{url('video-property')}}"><span class="icon"><i class="fas fa-video"></i></span>Videos of Property</a>
                                </h4>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a href="{{url('market-details')}}"><span class="icon"><i class="fas fa-shopping-basket"></i></span>Market Details of Site</a>
                                </h4>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a href="{{url('apartment')}}"><span class="icon"><i class="far fa-building"></i></span>Market Details of <span style="display: block;margin-left:26px">Apartments</span>
                                    </a>
                                </h4>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a href="{{url('house-details')}}"><span class="icon"><i class="fas fa-house-damage"></i></span>Details of House</a>
                                </h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                <div class="col-md-9 col-sm-9">
                    <div class="well">
                    <video width="100%" height="340" controls>
                      <source src="{{ asset('\uploads\videos\active_realty.mp4') }}" type="video/mp4">
                   </video>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Asperiores at dicta, dolorum, est fuga id natus optio pariatur quam quasi quisquam repellat repudiandae rerum sed, tenetur totam ullam? Debitis, molestiae?
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Asperiores at dicta, dolorum, est fuga id natus optio pariatur quam quasi quisquam repellat repudiandae rerum sed, tenetur totam ullam? Debitis, molestiae?
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Asperiores at dicta, dolorum, est fuga id natus optio pariatur quam quasi quisquam repellat repudiandae rerum sed, tenetur totam ullam? Debitis, molestiae?
                    </p>
               </div>
            </div>
        </div>
    </div>
@endsection