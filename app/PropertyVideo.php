<?php

namespace app;

use Illuminate\Database\Eloquent\Model;

class PropertyVideo extends Model
{
    protected $guarded = [];
    protected $table = 'property_videos';

    public function property(){
        return $this->belongsTo('app\Property');
    }
}
