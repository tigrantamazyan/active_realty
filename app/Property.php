<?php

namespace app;

use Illuminate\Database\Eloquent\Model;
use app\User;

class Property extends Model
{
    protected $table = 'properties';
    protected $fillable = ['user_id','name','price','size','bedrooms','location','description'];

    public function user(){
       return $this->belongsTo('app\User');
    }

    public function photos(){
        return $this->hasMany('app\PropertyPhoto');
    }

    public function videos(){
        return $this->hasMany('app\PropertyVideo');
    }
}
