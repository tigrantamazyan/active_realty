<?php

namespace app\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

class AdminController extends Controller
{

    public function index(){
        return view('pages.upload');
    }

    public function upload(Request $request)
    {
        if($request->file('file')){
            $file = $request->file('file');
            $filename = $file->getClientOriginalName();
            $path = public_path().'\uploads\videos';
            try {
                $file->move($path, $filename);
               return $path;
            } catch (\Exception $e) {
                var_dump($e->getMessage());
            }
        }

    }

/*    public function getVideo(Request $request,$video)
    {
        return $video;
        $fileContents = public_path().'\uploads\videos'.$video;
        $response = Response::make($fileContents, 200);
        $response->header('Content-Type', "video/mp4");
        return $response;
    }*/
}
