<?php

namespace app\Http\Controllers;

use app\Http\Requests\PropertyFormValidationRequest;
use app\Property;
use app\PropertyPhoto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class UserController extends Controller
{
    public function index(Request $request){
         $user = Auth::user()->load(['properties' => function ($query) {
             $query->with('photos', 'videos');
         }]);
         $properties = $user->properties;
         return view('profile/home')->with(compact('user','properties'));
    }

    public function create(){

        return view('profile/create');
    }

    public function store(PropertyFormValidationRequest $request){
        $property = Property::create([
            'user_id' => Auth::id(),
            'name' => $request->input('name'),
            'size' => $request->input('size'),
            'price' => $request->input('price'),
            'bedrooms' => $request->input('bedrooms'),
            'location' => $request->input('location'),
            'description' => $request->input('description'),
        ]);
        if($photos = $request->file('images')){
            foreach($photos as $photo){
                $filename = $photo->getClientOriginalName().'_'.time();
                $path = public_path().'\uploads\images';
                try {
                    $photo->move($path, $filename);
                    $property->photos()->create(['name' => $filename]);
                } catch (\Exception $e) {
                    var_dump($e->getMessage());
                }
            }
        }
        if($video = $request->file('video')){
            $filename = $video->getClientOriginalName().'_'.time();
            $path = public_path().'\uploads\videos';
            try {
                $video->move($path, $filename);
                $property->videos()->create(['name' => $filename]);
            } catch (\Exception $e) {
                var_dump($e->getMessage());
            }
        }
        return redirect()->back()->with(['message'=>'Property created successfully']);
    }

    public function edit(Request $request,$id){
        $property = Property::where('id',$id)->with('photos', 'videos')->first();
        return view('profile/single')->with(compact('property'));
    }
}
