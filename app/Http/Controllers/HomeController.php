<?php

namespace app\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
/*    public function __construct()
    {
        $this->middleware('auth');
    }*/

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages/home');
    }

    public function site_plans()
    {
        return view('pages/site-plans');
    }

    public function geology_report()
    {
        return view('pages/geology-report');
    }

    public function geology_plans()
    {
        return view('pages/geology-plans');
    }

    public function seismic_report()
    {
        return view('pages/seismic-report');
    }

    public function survey_topo()
    {
        return view('pages/survey-topo');
    }

    public function about(){
        return view('pages/about');
    }

    public function house_details(){
        return view('pages/house-details');
    }

    public function video_property(){
        return view('pages/video-property');
    }

    public function images_property(){
        return view('pages/images-property');
    }

    public function market_details(){
        return view('pages/market-details');
    }

    public function apartment(){
        return view('pages/apartment');
    }

}
