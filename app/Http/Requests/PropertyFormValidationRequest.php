<?php

namespace app\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PropertyFormValidationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'price' => 'required|integer',
            'size' => 'required|integer',
            'bedrooms' => 'required|integer',
            'location' => 'required|string|max:255',
            'description' => 'required|string',
            'images' => 'required',
//            'video' => 'mimes:mp4,mp3,mov,ogg | max:200000'
        ];
    }
}
