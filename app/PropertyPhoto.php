<?php

namespace app;

use Illuminate\Database\Eloquent\Model;

class PropertyPhoto extends Model
{
    protected $guarded = [];
    protected $table = 'property_photos';

    public function property(){
        return $this->belongsTo('app\Property');
    }
}
