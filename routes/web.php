<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/',function (){
  return view('main');
});

Auth::routes(['verify' => true]);
Route::get('profile', function () {
    // Only verified users may enter...
})->middleware('verified');

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/admin/upload','AdminController@index')->name('admin/upload');
Route::post('/admin/upload','AdminController@upload')->name('admin/upload');
//Route::get('get-video/{video}', 'AdminController@getVideo')->name('getVideo');

//Site pages//

Route::get('/about', 'HomeController@about')->name('about');
Route::get('/site-plans', 'HomeController@site_plans')->name('site-plans');
Route::get('/geology-report', 'HomeController@geology_report')->name('geology-report');
Route::get('/geology-plans', 'HomeController@geology_plans')->name('geology-plans');
Route::get('/seismic-report', 'HomeController@seismic_report')->name('seismic-report');
Route::get('/survey-topo', 'HomeController@survey_topo')->name('survey-topo');
Route::get('/house-details', 'HomeController@house_details')->name('house-details');
Route::get('/video-property', 'HomeController@video_property')->name('video-property');
Route::get('/images-property', 'HomeController@images_property')->name('images-property');
Route::get('/market-details', 'HomeController@market_details')->name('market-details');
Route::get('/apartment', 'HomeController@apartment')->name('apartment');
Route::get('/house-details', 'HomeController@house_details')->name('house-details');

////////////////////

//////profile//////////

Route::group(['prefix' => 'profile',  'middleware' => 'auth'], function(){
    Route::get('/','UserController@index');
    Route::get('create','UserController@create')->name('create');
    Route::post('store','UserController@store')->name('store');
    Route::get('edit/{id}','UserController@edit')->name('edit');
});


/////////////